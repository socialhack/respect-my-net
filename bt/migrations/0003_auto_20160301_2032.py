# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

def archive_old(apps, schema_editor):
    Violation = apps.get_model("bt", "Violation")

    for violation in Violation.objects.all():
        violation.old = True
        violation.save()

class Migration(migrations.Migration):

    dependencies = [
        ('bt', '0002_violation_old'),
    ]

    operations = [
            migrations.RunPython(archive_old),
    ]
