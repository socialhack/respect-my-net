from django import forms
from django_comments.forms import CommentForm
from django_comments.models import Comment
from django.utils.translation import ugettext_lazy as _
from captcha.fields import CaptchaField

class CommentFormWithCaptcha(CommentForm):
    captcha = CaptchaField(label=_("In order to protect against spam, please fill in the result of the following calculation. (note the + and the * are somewhat confusing)"))

    def get_comment_model(self):
        return Comment
